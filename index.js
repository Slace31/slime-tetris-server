let lobbies = {}
let players = {}

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

const ACTIONS = {
  MOVE_LEFT: 'moveLeft',
  MOVE_RIGHT: 'moveRight',
  ROTATE_LEFT: 'rotateLeft',
  ROTATE_RIGHT: 'rotateight',
  GO_DOWN: 'goDown'
}

const TICKS = 8

const launchGame = lobbyId => {
  lobbies[lobbyId].gameState = {}
  lobbies[lobbyId].gameState.speed = 2
  lobbies[lobbyId].gameState.tickNumber = 0
  lobbies[lobbyId].gameState.tickBasedActionsCount = 0
  lobbies[lobbyId].gameState.players = []
  Object.keys(lobbies[lobbyId].players).forEach(player => {
    lobbies[lobbyId].gameState.players.push({
      id: player,
      score: 0,
      lockedTicksCount: 0,
      nextAction: null,
      enemySlimes: 0,
      nextCouple: randomCouple(),
      actualCouple: null,
      moving: [],
      destroying: [],
      combo: [],
      board: emptyBoard()
    })
  })
  synchronize(lobbies[lobbyId])
  clearTimeout(lobbies[lobbyId].timeOut)
  lobbies[lobbyId].timeOut = setTimeout(() => {
    lobbies[lobbyId].gameLoop = setInterval(() => {
      if (lobbies[lobbyId] && lobbies[lobbyId].gameState && lobbies[lobbyId].gameState.players) {
        update(lobbies[lobbyId])
        synchronize(lobbies[lobbyId])
      }
    }, 1000 / TICKS)
  }, 3000)
  console.log('game for lobby [' + lobbyId + '] will start in 3s')
}



const removeActualCoupleFromBoard = (board, actualCouple) => {
  board[actualCouple[0].y][actualCouple[0].x] = -1
  board[actualCouple[1].y][actualCouple[1].x] = -1
}

const addActualCoupleToBoard = (board, actualCouple) => {
  board[actualCouple[0].y][actualCouple[0].x] = actualCouple[0].color
  board[actualCouple[1].y][actualCouple[1].x] = actualCouple[1].color
}

const processMoveLeft = (board, actualCouple) => {
  let minX = Math.min(actualCouple[0].x, actualCouple[1].x)
  if (minX > 0 && board[actualCouple[0].y][minX - 1] < 0 && board[actualCouple[1].y][minX - 1] < 0) {
    removeActualCoupleFromBoard(board, actualCouple)
    actualCouple[0].x--
    actualCouple[1].x--
    addActualCoupleToBoard(board, actualCouple)
  }
}

const processMoveRight = (board, actualCouple) => {
  let maxX = Math.max(actualCouple[0].x, actualCouple[1].x)
  if (maxX < (board[0].length - 1) && board[actualCouple[0].y][maxX + 1] < 0 && board[actualCouple[1].y][maxX + 1] < 0) {
    removeActualCoupleFromBoard(board, actualCouple)
    actualCouple[0].x++
    actualCouple[1].x++
    addActualCoupleToBoard(board, actualCouple)
  }
}

const processMoveDown = (board, actualCouple) => {
  let maxY = Math.max(actualCouple[0].y, actualCouple[1].y)

  if (maxY < (board.length - 1) && board[maxY + 1][actualCouple[0].x] < 0 && board[maxY + 1][actualCouple[1].x] < 0) {
    removeActualCoupleFromBoard(board, actualCouple)
    actualCouple[0].y++
    actualCouple[1].y++
    addActualCoupleToBoard(board, actualCouple)
  }
}

const processRotateRight = (board, actualCouple) => {
  let index
  if (actualCouple[0].y === actualCouple[1].y) {
    if (actualCouple[0].x > actualCouple[1].x) {
      index = 0
    } else {
      index = 1
    }
    if (board[actualCouple[index].y + 1] && board[actualCouple[index].y + 1][actualCouple[index].x - 1] === -1) {
      removeActualCoupleFromBoard(board, actualCouple)
      actualCouple[index].y++
      actualCouple[index].x--
      addActualCoupleToBoard(board, actualCouple)
    }

  } else {
    if (actualCouple[0].y < actualCouple[1].y) {
      index = 0
    } else {
      index = 1
    }

    if (board[actualCouple[index].y + 1] && board[actualCouple[index].y + 1][actualCouple[index].x + 1] === -1) {
      removeActualCoupleFromBoard(board, actualCouple)
      actualCouple[index].y++
      actualCouple[index].x++
      addActualCoupleToBoard(board, actualCouple)
    }
  }
}

const processRotateLeft = (board, actualCouple) => {
  let index
  if (actualCouple[0].y === actualCouple[1].y) {
    if (actualCouple[0].x < actualCouple[1].x) {
      index = 0
    } else {
      index = 1
    }
    if (board[actualCouple[index].y + 1] && board[actualCouple[index].y + 1][actualCouple[index].x + 1] === -1) {
      removeActualCoupleFromBoard(board, actualCouple)
      actualCouple[index].y++
      actualCouple[index].x++
      addActualCoupleToBoard(board, actualCouple)
    }

  } else {
    if (actualCouple[0].y < actualCouple[1].y) {
      index = 0
    } else {
      index = 1
    }

    if (board[actualCouple[index].y + 1] && board[actualCouple[index].y + 1][actualCouple[index].x - 1] === -1) {
      removeActualCoupleFromBoard(board, actualCouple)
      actualCouple[index].y++
      actualCouple[index].x--
      addActualCoupleToBoard(board, actualCouple)
    }
  }
}

const stopGame = lobbyId => {
  lobbies[lobbyId].gameState = {}
  clearInterval(lobbies[lobbyId].gameLoop)
  console.log('game of lobby [' + lobbyId + '] stoped')
}

const setPlayerNextCouple = player => {
  const lobbyId = players[player.id]
  const lobby = lobbies[lobbyId]
  if (player.board[0][3] > -1 || player.board[0][4] > -1) {
    io.to(lobbyId).emit('gameEnd', {
      loser: player.id
    })
    return stopGame(lobbyId)
  }

  player.actualCouple = [
    {
      x: 3,
      y: 0,
      color: player.nextCouple[0]
    },
    {
      x: 4,
      y: 0,
      color: player.nextCouple[1]
    }
  ]

  addActualCoupleToBoard(player.board, player.actualCouple)
  player.nextCouple = randomCouple()

  if (player.combo.length) {
    let opponentPlayerIndex = lobby.gameState.players.findIndex(x => x.id !== player.id)
    let slimes = 0
    player.combo.forEach((count, index) => {
      slimes += Math.floor(count / 3) * (index + 1)
    })

    lobby.gameState.players[opponentPlayerIndex].enemySlimes += slimes
    player.combo = []
  }
}

downAllSlimeThatCanDown = player => {
  let { board, actualCouple } = player

  player.moving = []

  for (let rowIndex = (board.length - 1); rowIndex >= 0; rowIndex--) {
    let row = board[rowIndex];
    for (let columnIndex = 0; columnIndex < row.length; columnIndex++) {
      const slime = row[columnIndex];
      let actualCoupleIndex = player.actualCouple ? player.actualCouple.findIndex(single => single.x === columnIndex && single.y === rowIndex) : -1
      if (slime > -1) {
        if (slime === 0 && board[rowIndex + 1] && board[rowIndex + 1][columnIndex] === -1) {
          let acutalRowIndex = board.length - 1
          while (acutalRowIndex > -1 && board[acutalRowIndex][columnIndex] !== -1) {
            acutalRowIndex--
          }
          if (acutalRowIndex > -1) {
            board[rowIndex][columnIndex] = -1
            player.moving.push(acutalRowIndex * row.length + columnIndex)
            board[acutalRowIndex][columnIndex] = 0
          }
        } else {
          if (board[rowIndex + 1] && board[rowIndex + 1][columnIndex] === -1) {
            player.moving.push((rowIndex + 1) * row.length + columnIndex)
            board[rowIndex + 1][columnIndex] = slime
            board[rowIndex][columnIndex] = -1
            if (player.actualCouple && player.actualCouple[actualCoupleIndex] && actualCoupleIndex > -1) {
              player.actualCouple[actualCoupleIndex].y++
            }
          } else {
            if (actualCoupleIndex > -1) {
              player.actualCouple = null
            }
          }
        }
      }
    }
  }
}

const destroyAllSlimeGroupedByThreeOrMore = player => {
  let { board, actualCouple } = player

  if (player.destroying.length) {
    let combo = 0

    player.destroying.forEach(index => {
      let rowIndex = Math.floor(index / board[0].length)
      let columnIndex = index % board[0].length

      if (board[rowIndex][columnIndex] > 0) {
        combo += 1.5
      }

      board[rowIndex][columnIndex] = -1
      if (actualCouple && actualCouple.find(single => single.x === columnIndex && single.y === rowIndex)) {
        player.actualCouple = null
      }
    })

    player.combo.push(Math.floor(combo))
  }

  player.destroying = []
  for (let rowIndex = (board.length - 1); rowIndex >= 0; rowIndex--) {
    let row = board[rowIndex];
    for (let columnIndex = 0; columnIndex < row.length; columnIndex++) {
      const slime = row[columnIndex];
      if (player.destroying.indexOf(rowIndex * row.length + columnIndex) === -1 && slime > 0) {
        let groupedSlimes = []
        getGroupedSlimes(groupedSlimes, slime, rowIndex, columnIndex, board, player)
        if (groupedSlimes.length >= 3) {
          player.destroying = [
            ...player.destroying,
            ...groupedSlimes
          ]
        }
      }
    }
  }

  let rocksToDestroy = []
  player.destroying.forEach(index => {
    let rowIndex = Math.floor(index / board[0].length)
    let columnIndex = index % board[0].length

    if (rowIndex + 1 < board.length && board[rowIndex + 1][columnIndex] === 0) {
      rocksToDestroy.push((rowIndex + 1) * board[0].length + columnIndex)
    }
    if (rowIndex - 1 > 0 && board[rowIndex - 1][columnIndex] === 0) {
      rocksToDestroy.push((rowIndex - 1) * board[0].length + columnIndex)
    }
    if (columnIndex + 1 < board[0].length && board[rowIndex][columnIndex + 1] === 0) {
      rocksToDestroy.push(rowIndex * board[0].length + columnIndex + 1)
    }
    if (columnIndex - 1 > 0 && board[rowIndex][columnIndex - 1] === 0) {
      rocksToDestroy.push(rowIndex * board[0].length + columnIndex - 1)
    }
  })

  player.destroying = [
    ...player.destroying,
    ...rocksToDestroy
  ]

  return player.destroying.length > 0
}

const getGroupedSlimes = (groupedSlimes, slime, rowIndex, columnIndex, board, player) => {
  if (slime < 0 || groupedSlimes.indexOf(rowIndex * board[0].length + columnIndex) !== -1) {
    return false
  } else {
    groupedSlimes.push(rowIndex * board[0].length + columnIndex)
  }

  if (rowIndex + 1 < board.length && board[rowIndex + 1][columnIndex] === slime) {
    getGroupedSlimes(groupedSlimes, slime, rowIndex + 1, columnIndex, board, player)
  }
  if (rowIndex - 1 > 0 && board[rowIndex - 1][columnIndex] === slime) {
    getGroupedSlimes(groupedSlimes, slime, rowIndex - 1, columnIndex, board, player)
  }
  if (columnIndex + 1 < board[0].length && board[rowIndex][columnIndex + 1] === slime) {
    getGroupedSlimes(groupedSlimes, slime, rowIndex, columnIndex + 1, board, player)
  }
  if (columnIndex - 1 > 0 && board[rowIndex][columnIndex - 1] === slime) {
    getGroupedSlimes(groupedSlimes, slime, rowIndex, columnIndex - 1, board, player)
  }
}

const addEnemySlimes = player => {
  if (!player.actualCouple && player.enemySlimes) {
    let rowOfSlimes = Math.floor(player.enemySlimes / player.board[0].length)
    let aloneSlimes = player.enemySlimes % player.board[0].length
    for (let rowIndex = 0; rowIndex < rowOfSlimes; rowIndex++) {
      for (let columnIndex = 0; columnIndex < player.board[0].length; columnIndex++) {
        if (player.board[rowIndex][columnIndex] === -1) {
          player.board[rowIndex][columnIndex] = 0
        }
      }
    }
    for (let index = 0; index < aloneSlimes; index++) {
      let columnIndex = Math.floor(Math.random() * (player.board[0].length - 1))
      if (player.board[0][columnIndex] === -1) {
        player.board[0][columnIndex] = 0
      }
    }
    player.enemySlimes = 0
    return true
  } else {
    return false
  }
}

const update = lobby => {
  // TODO
  let gameState = lobby.gameState

  let playAction = false
  if (gameState.tickNumber % (TICKS - 1) === 0) {
    gameState.tickNumber = 0
    gameState.tickBasedActionsCount = 0
  }
  if (gameState.tickNumber === gameState.tickBasedActionsCount * Math.floor(TICKS / gameState.speed)) {
    playAction = true
    gameState.tickBasedActionsCount++
  }

  gameState.tickNumber++

  gameState.players.forEach(player => {
    if (!player.lockedTicksCount && playAction) {
      if (player.destroying.length === 0) {
        downAllSlimeThatCanDown(player)
      }

      let skipSetPlayerNextCouple = player.destroying.length > 0
      if (player.moving.length === 0 && !player.actualCouple) {
        destroyAllSlimeGroupedByThreeOrMore(player)
      }

      if (player.moving.length === 0 && player.destroying.length === 0 && !player.actualCouple && !skipSetPlayerNextCouple && !addEnemySlimes(player)) {
        setPlayerNextCouple(player)
      }
    }

    player.lockedTicksCount = player.lockedTicksCount === 0 ? 0 : player.lockedTicksCount - 1
  })
}

const synchronize = lobby => {
  if (lobby.gameState && lobby.gameState.players) {
    io.to(lobby.id).emit('sync', lobby.gameState.players.map(player => ({
      board: {
        slimes: player.board,
        moving: [
          ...player.moving,
          ...(player.actualCouple && player.actualCouple.map ? player.actualCouple.map(single => single.y * player.board[0].length + single.x) : [])
        ],
        animated: player.destroying
      },
      nextCouple: player.nextCouple,
      enemySlimes: player.enemySlimes,
      name: lobby.players[player.id].name,
      id: player.id
    })))
  }
}

const randomSlime = () => {
  return 1 + Math.round(Math.random() * 3)
}

const randomCouple = () => {
  return [randomSlime(), randomSlime()]
}

const processAction = (playerId, nextAction) => {
  const lobbyId = players[playerId]
  if (lobbyId) {
    const playerIndex = Object.keys(lobbies[lobbyId].players).findIndex(key => key === playerId)
    if (lobbies[lobbyId].gameState && lobbies[lobbyId].gameState.players && lobbies[lobbyId].gameState.players[playerIndex]) {
      const player = lobbies[lobbyId].gameState.players[playerIndex]
      if (player.actualCouple) {
        switch (nextAction) {
          case ACTIONS.MOVE_LEFT:
            processMoveLeft(player.board, player.actualCouple)
            break;
          case ACTIONS.MOVE_RIGHT:
            processMoveRight(player.board, player.actualCouple)
            break;
          case ACTIONS.ROTATE_RIGHT:
            processRotateRight(player.board, player.actualCouple)
            break;
          case ACTIONS.ROTATE_LEFT:
            processRotateLeft(player.board, player.actualCouple)
            break;
          case ACTIONS.GO_DOWN:
            processMoveDown(player.board, player.actualCouple)
            break;
        }
      }
      synchronize(lobbies[lobbyId])
    }
  }
}

const moveLeft = (playerId) => processAction(playerId, ACTIONS.MOVE_LEFT)
const moveRight = (playerId) => processAction(playerId, ACTIONS.MOVE_RIGHT)
const rotateLeft = (playerId) => processAction(playerId, ACTIONS.ROTATE_LEFT)
const rotateRight = (playerId) => processAction(playerId, ACTIONS.ROTATE_RIGHT)
const goDown = (playerId) => processAction(playerId, ACTIONS.GO_DOWN)

const emptyBoard = () => {
  return [
    [-1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1],
  ]
}

io.on('connection', function (socket) {
  socket.on('addScreen', function (message) {
    let { lobbyId } = message
    if (!lobbyId) {
      lobbyId = 'default'
    }
    socket.join(lobbyId)
    console.log('Screen joined lobby [' + lobbyId + ']')
  });

  socket.on('addController', function (message) {
    let { nickname, lobbyId } = message
    if (!lobbyId) {
      lobbyId = 'default'
    }
    socket.leave(lobbyId)

    if (!lobbies[lobbyId]) {
      lobbies[lobbyId] = {
        id: lobbyId,
        players: {},
        gameState: {},
        timeOut: null,
        gameLoop: null
      }

    }

    if (Object.keys(lobbies[lobbyId].players).length < 2) {
      lobbies[lobbyId].players[socket.id] = {
        name: nickname
      }
      players[socket.id] = lobbyId
      console.log('Controller joined lobby [' + lobbyId + ']')
      io.to(lobbyId).emit('sync', Object.keys(lobbies[lobbyId].players).map(playerKey => ({
        name: lobbies[lobbyId].players[playerKey].name
      })))

      if (Object.keys(lobbies[lobbyId].players).length === 2) {
        launchGame(lobbyId)
      } else {
        synchronize(lobbies[lobbyId])
      }
    } else {
      socket.emit('connectionRefused')
    }
  });

  socket.on('moveLeft', function () {
    if (players[socket.id])
      moveLeft(socket.id)
  });
  socket.on('moveRight', function () {
    if (players[socket.id])
      moveRight(socket.id)
  });
  socket.on('rotateLeft', function () {
    if (players[socket.id])
      rotateLeft(socket.id)
  });
  socket.on('rotateRight', function () {
    if (players[socket.id])
      rotateRight(socket.id)
  });
  socket.on('goDown', function () {
    if (players[socket.id])
      goDown(socket.id)
  });

  socket.on('disconnect', function () {
    if (players[socket.id]) {
      setTimeout(() => {
        if (Object.keys(lobbies[players[socket.id]].players).length < 2) {
          delete lobbies[players[socket.id]]
          console.log('Removed lobby [' + players[socket.id] + '] after 5min of inactivity')
        }
      }, 1000 * 60 * 60 * 5)
      delete lobbies[players[socket.id]].players[socket.id]
      console.log('Controller leaved lobby [' + players[socket.id] + ']')
      stopGame(players[socket.id])
      delete players[socket.id]
    }
  });
});

http.listen(8084, function () {
  console.log('listening on *:8084');
});
