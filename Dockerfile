FROM node:latest

RUN mkdir api
WORKDIR /api

COPY package.json /api/package.json
RUN npm install

COPY . /api

EXPOSE 8084

CMD [ "npm", "start" ]